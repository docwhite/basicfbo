// Qt
#include <QGuiApplication>

// Custom
#include "Window.h"

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);
    Window window;
    window.show();
    window.setAnimating(true);
    return app.exec();
}
