// Native
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

// Qt
#include <QKeyEvent>
#include <QOpenGLShaderProgram>
#include <QTime>

// Custom
#include "Window.h"


Window::Window()
{
  QSurfaceFormat format;
  format.setSamples(16);
  this->resize(720, 720);
  this->setFormat(format);
}

void Window::initialize()
{
  glViewport(0, 0, width(), height());
  glEnable (GL_DEPTH_TEST);

  m_program_quad = new QOpenGLShaderProgram(this);
  m_program_quad->addShaderFromSourceFile(QOpenGLShader::Vertex, "shaders/quadVert.glsl");
  m_program_quad->addShaderFromSourceFile(QOpenGLShader::Fragment, "shaders/quadFrag.glsl");
  m_program_quad->link();

  m_program_scene = new QOpenGLShaderProgram(this);
  m_program_scene->addShaderFromSourceFile(QOpenGLShader::Vertex, "shaders/sceneVert.glsl");
  m_program_scene->addShaderFromSourceFile(QOpenGLShader::Fragment, "shaders/sceneFrag.glsl");
  m_program_scene->link();

  m_positions.push_back(-0.6f); m_positions.push_back(0.0f); m_positions.push_back(0.1f);
  m_positions.push_back(-0.3f); m_positions.push_back(0.0f); m_positions.push_back(0.1f);
  m_positions.push_back( 0.0f); m_positions.push_back(0.0f); m_positions.push_back(0.1f);
  m_positions.push_back( 0.3f); m_positions.push_back(0.0f); m_positions.push_back(0.1f);
  m_positions.push_back( 0.6f); m_positions.push_back(0.0f); m_positions.push_back(0.1f);

  m_quad.push_back(-1.0f); m_quad.push_back(-1.0f); m_quad.push_back(0.0f);
  m_quad.push_back( 1.0f); m_quad.push_back(-1.0f); m_quad.push_back(0.0f);
  m_quad.push_back( 1.0f); m_quad.push_back( 1.0f); m_quad.push_back(0.0f);
  m_quad.push_back( 1.0f); m_quad.push_back( 1.0f); m_quad.push_back(0.0f);
  m_quad.push_back(-1.0f); m_quad.push_back( 1.0f); m_quad.push_back(0.0f);
  m_quad.push_back(-1.0f); m_quad.push_back(-1.0f); m_quad.push_back(0.0f);

  m_VBO_particles = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  m_VBO_particles->create();

  m_VBO_quad = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  m_VBO_quad->create();

  glGenBuffers(1, &m_gBuffer);
  glBindFramebuffer(GL_FRAMEBUFFER, m_gBuffer);

  glGenTextures(1, &m_gPri);
  glBindTexture(GL_TEXTURE_2D, m_gPri);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 720, 720, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_gPri, 0);

  glGenTextures(1, &m_gSec);
  glBindTexture(GL_TEXTURE_2D, m_gSec);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 720, 720, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_gSec, 0);

  GLuint attachments[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
  glDrawBuffers(2, attachments);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Window::render()
{

    glBindFramebuffer(GL_FRAMEBUFFER, m_gBuffer);
        m_VBO_particles->bind();
        m_VBO_particles->allocate(&m_positions[0], 5 * 3 * sizeof(GLfloat));
        m_program_scene->bind();
        m_program_scene->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3);
        m_program_scene->enableAttributeArray("posAttr");
        glEnable(GL_POINT_SPRITE);
        glPointSize(64.0f);
        glDrawArrays(GL_POINTS, 0, 5);
        m_program_scene->release();
        m_VBO_particles->release();


    glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_POINT_SPRITE);
        glPointSize(0.0f);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_gPri);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, m_gSec);
        m_VBO_quad->bind();
        m_VBO_quad->allocate(&m_quad[0], 6 * 3 * sizeof(GLfloat));
        m_program_quad->bind();
        m_program_quad->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3);
        m_program_quad->enableAttributeArray("posAttr");
        m_program_quad->setUniformValue("gPri", 0);
        m_program_quad->setUniformValue("gSec", 1);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        m_program_quad->release();
        m_VBO_quad->release();
}

void Window::keyPressEvent(QKeyEvent *ev)
{
  if (ev->key() == Qt::Key_A)
    std::cout << "Key A has been pressed";
}
