TARGET = app

QT += core gui widgets openglextensions

CONFIG -= app_bundle

CONFIG += c++11

INCLUDEPATH += \
  $$PWD/include

OBJECTS_DIR = build/obj
MOC_DIR = build/moc

HEADERS += \
  include/Viewport.h \
  include/Window.h

SOURCES += \
  src/main.cpp \
  src/Viewport.cpp \
  src/Window.cpp

OTHER_FILES = \
  shaders/sceneVert.glsl \
  shaders/sceneFrag.glsl \
  shaders/quadVert.glsl \
  shaders/quadFrag.glsl

DISTFILES += \
    shaders/lines.glsl
