#version 450 core

uniform sampler2D gPri;
uniform sampler2D gSec;

out vec4 FragColor;

void main() {
    vec2 uv = gl_FragCoord.xy / vec2(720.0);
    vec4 gPriCol = texture2D(gPri, uv);
    vec4 gSecCol = texture2D(gSec, uv);
    FragColor = vec4(gPriCol.xyz, 1.0);
}
