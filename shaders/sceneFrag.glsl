#version 450 core

layout (location = 0) out vec4 gPriOut;
layout (location = 1) out vec4 gSecOut;

void main() {
    gPriOut = vec4(0.0, 1.0, 0.0, 0.0); // gPri
    gPriOut = vec4(0.0, 0.0, 1.0, 0.0); // gSec
}
