#ifndef WINDOW_H
#define WINDOW_H

// Qt
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

// Custom
#include "Viewport.h"

class Window : public Viewport
{
public:
    Window();
    void initialize() Q_DECL_OVERRIDE;
    void render() Q_DECL_OVERRIDE;

private:
    std::vector<GLfloat> m_positions;
    std::vector<GLfloat> m_quad;

    QOpenGLBuffer* m_VBO_particles;
    QOpenGLBuffer* m_VBO_quad;

    QOpenGLShaderProgram *m_program_quad;
    QOpenGLShaderProgram *m_program_scene;

    GLuint m_gBuffer;
    GLuint m_gPri;
    GLuint m_gSec;

protected:
    void keyPressEvent(QKeyEvent* ev);
};

#endif // PARTICLESWINDOW_H
